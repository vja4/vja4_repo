<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    include "class.php";
    $ShowProduct = new MainClass();
?>
<html>
    <head>
        <title>Product list</title>
        <style>
            div {
                float: left;
                margin: 10px;
                background-color: #00bfff;
                text-align: center;
                width: 100px;
                height: 120px;
                border: 1px double black;
            }
            body {
                background-color: #ADD8E6;
                
            }
            .container {
                width:auto;
                height:auto;
                position:absolute;
                left:25%;
                background-color: #00ffff;
            }
            .list {
                background-color: #0080ff;
                width:490px;
                height: auto;
            }
            .menu {
                width:190px;
                background-color:#0080ff;
                float:left;
            }
            a {
                color:white;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <center>
            <div class="list" align="center">
                <?php
                    $SKU = null;
                    $Name = null;
                    $Price = null;
                    $Type = null;
                    $Value = null;
                    $ShowProduct->ShowProducts($conn);
                ?>
            </div>
            <div class="menu">
                <form action="index.php" method="post">
                <ul>
                    <li><a href="add.php">Add new product</a></li>
                    <li><input type="submit" name="delete" value="delete"/></li>
                    <?php
                        if(isset($_POST['delete'])){
                            $ShowProduct->DeleteData();
                        }
                    ?>
                </ul>
                </form>
            </div>
            </center>
        </div>
        <?php $conn->close(); ?>
    </body>
</html>